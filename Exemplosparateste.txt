Vendedor

{
  "nome": "jorge",
  "cpf": "12121212",
  "email": "jorge@email.com",
  "telefone": "121212"
}

{
  "cpf": "13131313",
  "nome": "joao",
  "telefone": "131313",
  "email": "joao@email.com"
}


Produto

{
  "nome": "Caixa de Pregos",
  "preco": 150
}

{
  "nome": "Palet de Táboas de Madeira",
  "preco": 2350
}

Venda

{
  "dataVenda": "2022-11-18T19:12:14.326Z",
  "numeroDoPedido": 12,
  "vendedorId": 1,
  "produtoId": 1,
  "statusVenda": 0
}

{
  "dataVenda": "2022-11-18T19:12:14.326Z",
  "numeroDoPedido": 15,
  "vendedorId": 2,
  "produtoId": 2,
  "statusVenda": 0
}